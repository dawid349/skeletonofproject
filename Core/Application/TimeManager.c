
#include "TimeManager.h"


int debug_2=0;

//______________________________________________
void TimeManagerInit(TIM_HandleTypeDef * ChosenTimer);
void TimeControl(DelayTicks* Ticks);
static void IncreaseTicks(DelayTicks* Ticks);
//______________________________________________

void TimeManagerInit(TIM_HandleTypeDef * ChosenTimer)
{
	HAL_TIM_Base_Start_IT(ChosenTimer);
}

static void IncreaseTicks(DelayTicks* Ticks)
{
	/* Increase each tick */
	Ticks->InputsTick++;
	Ticks->CanSendFrameTick++;
	Ticks->CanBmsRqstTick++;
	Ticks->BtSendTick++;

}

void TimeControl(DelayTicks* Ticks)
{
	/* Functions increasing and control current ticks for each periodical action */
	debug_2++;

	if(Ticks->InputsTick >= INPUT_DELAY)
	{
		// TAKE SOME ACTIONS....
		Ticks->InputsTick = 0; // reset tick for an action

	}

	if(Ticks->CanBmsRqstTick >= CAN_BMS_RQST_DELAY)
	{
		CanRqstData(BMS_BASE_ADDR + 132); // request BMS statistics data once for 100 [ms]
		CanRqstData(BMS_BASE_ADDR + 133); // request BMS events data
		Ticks->CanBmsRqstTick = 0;
	}

	if(Ticks->CanSendFrameTick >= CAN_SEND_FRAME_DELAY)
	{
		// TAKE SOME ACTIONS....
		Ticks->CanSendFrameTick = 0; // reset tick for an action

	}

	if(Ticks->BtSendTick >= BT_DELAY)
	{
		// TAKE SOME ACTIONS....
		Ticks->BtSendTick = 0; // reset tick for an action

	}

	/* Increase each tick */
	IncreaseTicks(Ticks);

}


