/*
 * DataTypes.h
 *
 *  Created on: 25 lis 2020
 *      Author: Dawid
 */

#ifndef APPLICATION_DATATYPES_H_
#define APPLICATION_DATATYPES_H_

#include "stm32l4xx_hal.h"
#include "stdbool.h"

typedef enum	/* Charging state */
{
	NO_CHRG,		//No charging
	PRE_CH,		//Precharge state
	MAIN_CH,	//Main charge
	BALANCING,	//Balancing state
	FINISHED	//Charging finished
}Charging;

typedef struct	/* BMS Event data */
{
	volatile bool low_voltage_alarm;	//Critical low cells voltage
	volatile bool high_voltage_alarm;	//Critical high cells voltage
	volatile bool temp_alarm;			//Critical temperature
	volatile bool low_voltage;			//Low voltage, reducing power
	volatile bool high_current;			//Critical current
	volatile Charging chrg;
}BmsEvent;

typedef struct /* BMS data */
{
	volatile float voltage_total;	//Total voltage	[V]
	volatile float voltage_cellavg;	//Average cell voltage [V]
	volatile float capacity;			//TO DO !!!
	volatile float max_celltemp;		//Maximum temp of cell battery [oC]
	volatile float avg_celltemp;				//@@ ????????????
	volatile float consumption;		//TO DO !!!
	volatile float current;			//Discharge current/charge current [A]
	volatile float charge_left; 		//Left battery capacity [%]
	volatile float distance_left;			//@@ ????????????
	volatile float cycles;					//@@ ???????????? amount of charging cycles
	volatile float charge_time;				//@@ total time of charge [s]???
	volatile float distance_total;			//@@ from last battery charge

	volatile BmsEvent event;
	volatile uint8_t out_pin_status;
	volatile uint8_t charging_state;
}BmsData;

typedef struct	/* Motor controller data */
{
	volatile float current_pha;
	volatile float dc_current;
	volatile float motor_power;
	volatile float motor_temp;
	volatile float temp;
	volatile float v_actual;
	volatile float v_filtered;
	volatile float throttle;
}EmData;

typedef struct
{
	volatile BmsData bms;
	volatile EmData em;
	volatile float btapp_lat;		//Geo data from bluetooth app
	volatile float btapp_lon;
	volatile uint8_t drivingMode;
	volatile uint8_t maxSpeed;
	volatile uint8_t maxTorque;
	volatile bool autoLights;
}NetData;

typedef enum	/* Input signals */
{
	PRK_LGHT,
	DRV_LGHT,
	BEAM_LGHT,
	BRK_BAR,
	BRK_FOOT,
	IND_L,
	IND_R,
	EMR_LGHT,
	HORN,
	DRV_MODE,
	KCKSTND,
	NAV_L,
	NAV_R,
	NAV_M,

	IN_MAX	//MAX INPUT
}Inputs;

typedef struct {
	volatile uint16_t voltage_adc;
	volatile float temp_celsius;
}AdcData;

typedef struct /* Sensors data */
{
	volatile float temperature;
	volatile float gps_lat;		//Geo data from GPS device
	volatile float gps_lon;
	volatile uint8_t drivingMode;
	volatile float ntc_read;
	volatile bool inputs_data[IN_MAX - 1];
	volatile AdcData adc;
}SensorsData;

//volatile NetData NetworkData = {0};
//volatile SensorsData DataFromSensors = {0};

#endif /* APPLICATION_DATATYPES_H_ */
