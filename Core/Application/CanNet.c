/*
 * CanNet.c
 *
 *  Created on: 19 gru 2020
 *      Author: Dawid
 */

#include "CanNet.h"

volatile NetData NetworkData = {0};
BMS_stats Stat = NONE_STAT;
extern volatile uint8_t message; // TESTY


void CanReceiveData(volatile CanRxFrameID FrameId, volatile CanMsg * CanData)
{

	switch(FrameId)
		{
			case BAT_VOLTAGE:
			{
				NetworkData.bms.voltage_cellavg = CanData->RxData[2]*0.01+2.0;  // [V]
				NetworkData.bms.voltage_total = (CanData->RxData[4]+(CanData->RxData[3] << 8)+(CanData->RxData[6] << 16) + (CanData->RxData[5] << 24))*0.01;
			}
			break;


			case CELL_TEMP:
			{
				NetworkData.bms.avg_celltemp = CanData->RxData[2] - 100;    // in Celsius Degrees
				NetworkData.bms.max_celltemp = CanData->RxData[1] - 100;
			}
			break;

			case BAT_CHARGE:
			{
				/* Move MSB 8 bits -> add LSB -> cast at signed 16bit integer */
				NetworkData.bms.current = ((int16_t)((CanData->RxData[0]<<8) + CanData->RxData[1]))*0.1; // [A]
				NetworkData.bms.charge_left = CanData->RxData[6];	// [%]
			}
			break;

			case BAT_DISTANCE:
			{
				NetworkData.bms.distance_left = ((CanData->RxData[4]<<8) + CanData->RxData[5])*0.1; // [chosen distance unit -- I assumed km]
				NetworkData.bms.distance_total = ((CanData->RxData[6]<<8) + CanData->RxData[7])*0.1;
			}
			break;

			case BMS_STATS:
			{
				switch(CanData->RxData[1]) // switch depends on BMS statictic identyfier
				{

					case CHRG_CYCLES:
					{
							NetworkData.bms.cycles = CanData->RxData[5] + (CanData ->RxData[4] << 8);
					}
					break;

					case CHRG_TIME:
					{
							NetworkData.bms.charge_time = CanData->RxData[5]+(CanData->RxData[4] << 8)+(CanData->RxData[3] << 16) + (CanData->RxData[2] << 24);
					}
					break;
				}

			}
			break;

			case BMS_EVENTS:
			{
				if(CanData->RxData[1] == 0)  // only if it is event info data frame
				{
					switch(CanData->RxData[2]) // deal with data depending on event identifier
					{
						case 0:  // if there is no alarm occured
						{
							NetworkData.bms.event.chrg = NO_CHRG;
							NetworkData.bms.event.high_current = false;
							NetworkData.bms.event.low_voltage = false;
							NetworkData.bms.event.temp_alarm = false;
							NetworkData.bms.event.low_voltage_alarm = false;
							NetworkData.bms.event.high_voltage_alarm = false;
						}
						break;

						case 4: /* CELLS VOLTAGE CRITICALLY LOW */
						{
							NetworkData.bms.event.low_voltage_alarm = true;  // voltage alarm occured
						}
						break;

						case 6: /* CELLS VOLTAGE CRITICALLY HIGH */
						{
							NetworkData.bms.event.high_voltage_alarm = true; // voltage alarm occured
						}
						break;

						case 8: /* DISCHARGE CURRENT CRITICALLY HIGH */
						{
							NetworkData.bms.event.high_current = true;
						}
						break;

						case 16: /* WARNING - LOW VOLTAGE - REDUCING POWER */
						{
							NetworkData.bms.event.low_voltage = true;
						}
						break;

						case 42: /* CELL TEMPERATURE CRITICALLY HIGH */
						{
							NetworkData.bms.event.temp_alarm = true;
						}
						break;

						case 25: /* STARTED PRECHARGING STAGE */
						{
							NetworkData.bms.event.chrg = PRE_CH;
						}
						break;

						case 26: /* STARTED MAIN CHARGING STAGE */
						{
							NetworkData.bms.event.chrg = MAIN_CH;
						}
						break;

						case 27: /* STARTED BAlANCING STAGE */
						{
							NetworkData.bms.event.chrg = BALANCING;
						}
							break;

						case 28: /* CHARGING FINISHED */
						{
							NetworkData.bms.event.chrg = FINISHED;
						}
						break;
					}

				}

			}
			break;

			default:
			{
				// do nothing
			}
			break;


		}

}
